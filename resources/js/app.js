import Vue from 'vue'
import router from './router'
import Navigation from './components/Navigation'

const app = new Vue({
    router,
    components: {
        Navigation
    }
}).$mount('#app')