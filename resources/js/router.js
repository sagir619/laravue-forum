import Vue from 'vue'
import Router from 'vue-router'
import Home from './pages/Home'
import Login from './pages/login'
import Register from './pages/Register'
import AfterLogin from './pages/AfterLogin'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/login',
            name: 'login',
            component: Login
        },
        {
            path: '/register',
            name: 'register',
            component: Register
        },
        {
            path: '/after-login',
            name: 'after-login',
            component: AfterLogin
        }
    ]
})