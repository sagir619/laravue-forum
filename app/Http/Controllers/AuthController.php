<?php

namespace App\Http\Controllers;

use Hash;
use App\User;
use Illuminate\Http\Request;
use GuzzleHttp\Client as Http;
use GuzzleHttp\Exception\BadResponseException as HttpException;

class AuthController extends Controller
{
    public function register (Request $request)
    {
        $request->validate([
            'name'     => 'required|min:3|max: 30',
            'email'    => 'required|email|unique:users,email',
            'password' => 'required|min:6|max:16'
        ]);

        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);

        return $user->save()
            ? response()->json('You have registered successfully', 200)
            : response()->json('Something went wrong. please try again', 500);
    }

    public function login (Request $request)
    {
        $http = new Http;

        try {
            $response = $http->post('http://localhost:8000/oauth/token', [
                [
                    'grant_type' => 'password',
                    'client_id'  => 2,
                    'client_secret' => '5aa0IJl8FT3PG1N6YxuruC0wjgscJUJzaiEf8yp9',
                    'username' => $request->email,
                    'password' => $request->password
                ]
            ]);

            return $response;
        } catch (HttpException $error) {
            $code = $error->getCode();

            switch ($code) {
                case 400:
                    return response()->json('Invalid Request.', $code);
                case 401:
                    return response()->json('Incorrent email or password.', $code);
                default:
                    return $error;
            }
        }
    }
}
